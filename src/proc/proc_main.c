/*
 * kmod_template
 * Copyright (C) Mehmet Akif Tasova <makiftasova@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <linux/compiler.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/uaccess.h> /* for copy_from_user */
#include <linux/spinlock.h>

#include "kmod_template.h"
#include "kmod_proc.h"

#define KMOD_PROC_PERMISSIONS (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH)
#define KMOD_PROC_ALLOC_STRATEGY (GFP_KERNEL)

struct kmod_proc_buffer {
	char *buf;
	size_t buf_len;
};

static int kmod_proc_try_buffer_alloc(struct kmod_proc_buffer *pbuf, size_t len)
{
	if (unlikely(pbuf->buf || !len))
		return -EINVAL;

	pbuf->buf = kcalloc(len, sizeof(char), KMOD_PROC_ALLOC_STRATEGY);
	if (unlikely(!pbuf->buf))
		return -ENOMEM;

	pbuf->buf_len = len;

	return 0;
}

static int kmod_proc_try_buffer_realloc(struct kmod_proc_buffer *pbuf, size_t new_len)
{
	char *new_buf = NULL;

	++new_len;

	/* free abundant memory if possible */
	if (2 * new_len <= pbuf->buf_len)
		goto resize;

	if (new_len <= pbuf->buf_len)
		return 0;

resize:
	new_buf = krealloc(pbuf->buf, new_len * sizeof(char), KMOD_PROC_ALLOC_STRATEGY);
	if (unlikely(!new_buf))
		return -ENOMEM;

	pbuf->buf = new_buf;
	pbuf->buf_len = new_len + 1;
	return 0;
}

static void kmod_proc_buffer_free(struct kmod_proc_buffer *pbuf)
{
	pbuf->buf_len = 0;
	if (likely(pbuf->buf)) {
		kfree(pbuf->buf);
		pbuf->buf = NULL;
	}
}

struct kmod_proc_buffer proc_buffer = { .buf = NULL, .buf_len = 0 };
DEFINE_RWLOCK(rwlock);

static int kmod_proc_show(struct seq_file *m, void *v)
{
	read_lock(&rwlock);
	seq_printf(m, "%s\n", proc_buffer.buf);
	read_unlock(&rwlock);
	return 0;
}

static int kmod_proc_open(struct inode *inode, struct file *file)
{
	return single_open(file, kmod_proc_show, NULL);
}

static ssize_t kmod_proc_write(struct file *file, const char *ubuf, size_t count, loff_t *ppos)
{
	ssize_t ret = 0;

	write_lock(&rwlock);

	ret = kmod_proc_try_buffer_realloc(&proc_buffer, count);
	if (unlikely(ret < 0))
		goto exit;

	copy_from_user(proc_buffer.buf, ubuf, count);
	ret = (ssize_t) count;
	proc_buffer.buf[count] = '\0';

exit:
	write_unlock(&rwlock);

	*ppos = count;
	return ret;
}

static const struct file_operations kmod_proc_fops = {
	.owner = THIS_MODULE,
	.open = kmod_proc_open,
	.write = kmod_proc_write,
	.read = seq_read,
	.llseek = seq_lseek,
	.release = single_release,
};

int __init kmod_proc_init(void)
{
	int ret = 0;
	KMOD_TMPL_INFO(KMOD_PROC ": Initializing...");

	/* setup string buffer*/
	ret = kmod_proc_try_buffer_alloc(&proc_buffer, KMOD_PROC_INIT_BUF_LEN);
	if (ret < 0) {
		KMOD_TMPL_CRIT(KMOD_PROC ": Failed to allocate buffer with error %d", -ret);
		return ret;
	}
	snprintf(proc_buffer.buf, proc_buffer.buf_len, KMOD_PROC ": Hello proc!");

	/* create proc entry */
	if (proc_create(KMOD_PROC_NAME, KMOD_PROC_PERMISSIONS, NULL, &kmod_proc_fops) == NULL) {
		ret = -ENOMEM;
		KMOD_TMPL_CRIT(KMOD_PROC ": proc_create failed with error %d", -ret);
		kmod_proc_buffer_free(&proc_buffer);
		return ret;
	}

	KMOD_TMPL_INFO(KMOD_PROC ": Hello!");
	return 0;
}

void __exit kmod_proc_cleanup(void)
{
	kmod_proc_buffer_free(&proc_buffer);
	remove_proc_entry(KMOD_PROC_NAME, NULL);
	KMOD_TMPL_INFO(KMOD_PROC_NAME ": Goodbye!");
}
