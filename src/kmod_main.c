/*
 * kmod_template
 * Copyright (C) Mehmet Akif Tasova <makiftasova@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>

#include "kmod_template.h"

#define KMOD_KMOD "KMOD_MAIN"

#if defined(BUILD_PROC)
#include "kmod_proc.h"
#endif /* BUILD_PROC */

#if defined(BUILD_CHAR)
#include "kmod_char.h"
#endif /* BUILD_CHAR */

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Mehmet Akif Tasova <makiftasova@gmail.com>");
MODULE_DESCRIPTION("An out of tree kernel module template");

static int __init kmod_template_init(void)
{
	int ret = 0;
#if defined(BUILD_PROC)
	ret = kmod_proc_init();
	if (ret)
		return ret;
#endif /* BUILD_PROC */

#if defined(BUILD_CHAR)
	ret = kmod_char_init();
	if (ret)
		return ret;
#endif /* BUILD_CHAR */

	KMOD_TMPL_INFO(KMOD_KMOD ": Hello, I'm a kernel module");

	return ret;
}

static void __exit kmod_template_cleanup(void)
{
#if defined(BUILD_CHAR)
	kmod_char_cleanup();
#endif /* BUILD_CHAR */

#if defined(BUILD_PROC)
	kmod_proc_cleanup();
#endif /* BUILD_PROC */

	KMOD_TMPL_INFO(KMOD_KMOD ": Goodbye!");
}

module_init(kmod_template_init);
module_exit(kmod_template_cleanup);

