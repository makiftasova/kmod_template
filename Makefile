MODULE_NAME = kmod_template

KVER  := $(shell uname -r)
KDIR  := /lib/modules/$(KVER)/build

EXTRA_CFLAGS-y             := -I$(PWD)/src -I$(PWD)/include
EXTRA_CFLAGS-y             += -Wall -Wextra -Werror -Wno-unused
EXTRA_CFLAGS-$(BUILD_PROC) += -DBUILD_PROC=1
EXTRA_CFLAGS-$(BUILD_CHAR) += -DBUILD_CHAR=1

EXTRA_CFLAGS               := $(EXTRA_CFLAGS-y)

$(info $(EXTRA_CFLAGS))

SRC-y             := src/kmod_main.c
SRC-$(BUILD_PROC) += src/proc/proc_main.c
SRC-$(BUILD_CHAR) += src/char/char_main.c

SRC := $(SRC-y)

$(MODULE_NAME)-objs := $(SRC:.c=.o)

obj-m := $(MODULE_NAME).o


all:
	$(MAKE) -C $(KDIR) M=$(PWD) modules

clean:
	$(MAKE) -C $(KDIR) M=$(PWD) clean
