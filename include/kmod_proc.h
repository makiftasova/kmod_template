/*
 * kmod_template
 * Copyright (C) Mehmet Akif Tasova <makiftasova@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __KMOD_PROC_H__
#define __KMOD_PROC_H__

#include <linux/init.h>
#include <linux/ctype.h>

#define KMOD_PROC "KMOD_PROC"
#define KMOD_PROC_NAME "kmod_proc"

#define KMOD_PROC_INIT_BUF_LEN 32

int __init kmod_proc_init(void);

void __exit kmod_proc_cleanup(void);

#endif /* __KMOD_PROC_H__ */
