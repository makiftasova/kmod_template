/*
 * kmod_template
 * Copyright (C) Mehmet Akif Tasova <makiftasova@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __KMOD_TEMPLATE_H__
#define __KMOD_TEMPLATE_H__

#define KMOD_TMPL_MODULE_NAME "KMOD_TEMPLATE"

#define _KMOD_TMPL_PRINTK(once, level, fmt, ...)                                                   \
	do {                                                                                       \
		printk##once(KERN_##level "[" KMOD_TMPL_MODULE_NAME "] " fmt "\n", ##__VA_ARGS__); \
	} while (0)

#define KMOD_TMPL_EMERG(fmt, ...) _KMOD_TMPL_PRINTK(, EMERG, fmt, ##__VA_ARGS__)
#define KMOD_TMPL_ALERT(fmt, ...) _KMOD_TMPL_PRINTK(, ALERT, fmt, ##__VA_ARGS__)
#define KMOD_TMPL_CRIT(fmt, ...) _KMOD_TMPL_PRINTK(, CRIT, fmt, ##__VA_ARGS__)
#define KMOD_TMPL_ERR(fmt, ...) _KMOD_TMPL_PRINTK(, ERR, fmt, ##__VA_ARGS__)
#define KMOD_TMPL_WARN(fmt, ...) _KMOD_TMPL_PRINTK(, WARN, fmt, ##__VA_ARGS__)
#define KMOD_TMPL_NOTICE(fmt, ...) _KMOD_TMPL_PRINTK(, NOTICE, fmt, ##__VA_ARGS__)
#define KMOD_TMPL_INFO(fmt, ...) _KMOD_TMPL_PRINTK(, INFO, fmt, ##__VA_ARGS__)

#define KMOD_TMPL_EMERG_ONCE(fmt, ...) _KMOD_TMPL_PRINTK(_once, EMERG, fmt, ##__VA_ARGS__)
#define KMOD_TMPL_ALERT_ONCE(fmt, ...) _KMOD_TMPL_PRINTK(_once, ALERT, fmt, ##__VA_ARGS__)
#define KMOD_TMPL_CRIT_ONCE(fmt, ...) _KMOD_TMPL_PRINTK(_once, CRIT, fmt, ##__VA_ARGS__)
#define KMOD_TMPL_ERR_ONCE(fmt, ...) _KMOD_TMPL_PRINTK(_once, ERR, fmt, ##__VA_ARGS__)
#define KMOD_TMPL_WARN_ONCE(fmt, ...) _KMOD_TMPL_PRINTK(_once, WARN, fmt, ##__VA_ARGS__)
#define KMOD_TMPL_NOTICE_ONCE(fmt, ...) _KMOD_TMPL_PRINTK(_once, NOTICE, fmt, ##__VA_ARGS__)
#define KMOD_TMPL_INFO_ONCE(fmt, ...) _KMOD_TMPL_PRINTK(_once, INFO, fmt, ##__VA_ARGS__)

#endif /* __KMOD_TEMPLATE_H__ */
